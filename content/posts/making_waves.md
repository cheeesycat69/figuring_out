+++
title = 'Making waves'
date = 2024-04-18T11:35:52+05:30
draft = false
+++

How to wow people. With every small thing you do I think you need to think if there is scope to wow someone
and if you can wow them, thats when you start earning trust and people start looking up to you, like 
how did he / she do it ? Look out for wowing people around you every minute move. Make do things which they dont expect
or forsee or didnt think themselves and arent sure how exactly we did it.
