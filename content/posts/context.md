+++
title = 'Context'
date = 2024-04-20T10:15:14+05:30
draft = false
+++

Its always a special power to have good context, most of the times we tend to
go with blind assumptions into things and later realize new things and wish we knew better.
Spending time to get context is underrated. Maybe before diving into anything,
it could be useful to understand the underlying motive or expected result.
